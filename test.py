"""
import time
import math
from openal.audio import SoundSink, SoundSource
from openal.loaders import load_wav_file

if __name__ == "__main__":
    sink = SoundSink()
    sink.activate()
    source = SoundSource(position=[0, 0, 0])
    source.looping = True
    data = load_wav_file("./sounds/Blip_Select.wav")
    source.queue(data)
    sink.play(source)
    t = 0
    while True:
        x_pos = 5*math.sin(math.radians(t))
        source.position = [x_pos, source.position[1], source.position[2]]
        sink.update()
        print("playing at %r" % source.position)
        time.sleep(0.1)
        t += 5
"""
import base64
import json
import logging
import numpy as np
from flask import Flask
from flask_sockets import Sockets
import io ,time

import soundfile as sf
app = Flask(__name__)
sockets = Sockets(app)

HTTP_SERVER_PORT = 5000

@sockets.route('/media')
def echo(ws):
    app.logger.info("Connection accepted")
    # A lot of messages will be sent rapidly. We'll stop showing after the first one.
    has_seen_media = False
    message_count = 0
    f=open("prueba.webm","wb")
    tiempo=time.time()
    while not ws.closed:
        message = ws.receive()
        if message is None:
            app.logger.info("No message received...")
            continue

        # Messages are a JSON encoded string
        data={"event":"otra cosa"}
      
        if type(message)==bytearray:
            
            if not f.closed:
                f.write(message)
            
            if time.time()-tiempo>12:
                print("cerrado")
                f.close()
            
        else:
            data = json.loads(message)
        try:

            # Using the event type you can determine what type of message you are receiving
            if data['event'] == "connected":
                app.logger.info("Connected Message received: {}".format(message))
            if data['event'] == "start":
                app.logger.info("Start Message received: {}".format(message))
            if data['event'] == "media":
                if not has_seen_media:
                    app.logger.info("Media message: {}".format(message))
                    payload = data['media']['payload']
                    app.logger.info("Payload is: {}".format(payload))
                    chunk = base64.b64decode(payload)
                    app.logger.info("That's {} bytes".format(len(chunk)))
                    app.logger.info("Additional media messages from WebSocket are being suppressed....")
                    has_seen_media = True
            if data['event'] == "closed":
                app.logger.info("Closed Message received: {}".format(message))
                break
            message_count += 1
        except Exception as e:
            print(e)
        print(type(message))
        print("uuuuuuus")



    app.logger.info("Connection closed. Received a total of {} messages".format(message_count))


if __name__ == '__main__':
    app.logger.setLevel(logging.DEBUG)
    from gevent import pywsgi
    from geventwebsocket.handler import WebSocketHandler

    server = pywsgi.WSGIServer(('', HTTP_SERVER_PORT), app, handler_class=WebSocketHandler)
    print("Server listening on: http://localhost:" + str(HTTP_SERVER_PORT))
    server.serve_forever()
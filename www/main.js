var isRecording = false, encode = false;
var wsh = new WebSocket( 'ws://' + window.location.href.split( '/' )[2] + '/ws' );
console.log("xxxxxxxxxx",'ws://' + window.location.href.split( '/' )[2] + '/ws' )
function toUTF8Array(str) {
    var utf8 = [];
    for (var i=0; i < str.length; i++) {
        var charcode = str.charCodeAt(i);
        if (charcode < 0x80) utf8.push(charcode);
        else if (charcode < 0x800) {
            utf8.push(0xc0 | (charcode >> 6), 
                      0x80 | (charcode & 0x3f));
        }
        else if (charcode < 0xd800 || charcode >= 0xe000) {
            utf8.push(0xe0 | (charcode >> 12), 
                      0x80 | ((charcode>>6) & 0x3f), 
                      0x80 | (charcode & 0x3f));
        }
        // surrogate pair
        else {
            i++;
            // UTF-16 encodes 0x10000-0x10FFFF by
            // subtracting 0x10000 and splitting the
            // 20 bits of 0x0-0xFFFFF into two halves
            charcode = 0x10000 + (((charcode & 0x3ff)<<10)
                      | (str.charCodeAt(i) & 0x3ff));
            utf8.push(0xf0 | (charcode >>18), 
                      0x80 | ((charcode>>12) & 0x3f), 
                      0x80 | ((charcode>>6) & 0x3f), 
                      0x80 | (charcode & 0x3f));
        }
    }
    return utf8;
}
var audioCtx;



var base64ToBuffer = function (buffer) {
    var binary = window.atob(buffer);
    var buffer = new ArrayBuffer(binary.length);
    
    //var bytes = new Uint8Array(buffer);
    var bytes = new Int16Array(buffer);
    //var bytes = new  Float32Array(buffer);
    for (var i = 0; i < buffer.byteLength; i++) {
        bytes[i] = binary.charCodeAt(i) & 0xFF;
    }

    //return buffer;
    return bytes;
};

function processConcatenatedFile( data ) {

  var bb = new DataView( data );
  var offset = 0;

  while( offset < bb.byteLength ) {

    var length = bb.getUint32( offset, true );
    offset += 4;
    var sound = extractBuffer( data, offset, length );
    offset += length;

    createSoundWithBuffer( sound.buffer );

  }

}
function extractBuffer( src, offset, length ) {

  var dstU8 = new Uint8Array( length );
  var srcU8 = new Uint8Array( src, offset, length );
  dstU8.set( srcU8 );
  return dstU8;

}
function createSoundWithBuffer( buffer ) {

  /*
    This audio context is unprefixed!
  */
  var context = new AudioContext();

  var audioSource = context.createBufferSource();
  audioSource.connect( context.destination );

  context.decodeAudioData( buffer, function( res ) {

    audioSource.buffer = res;

    /*
       Do something with the sound, for instance, play it.
       Watch out: all the sounds will sound at the same time!
    */
      audioSource.noteOn( 0 );

  } );

}

function playSound(buffer, playTime) {
var source = audioCtx.createBufferSource(); //Create a new BufferSource fr the
source.buffer = buffer; // Put the sample content into the buffer
source.start(playTime); // Set the starting time of the sample to the scheduled play time
//source.connect(analyserNode); //Connect the source to the visualiser
source.connect(audioCtx.destination); // Also Connect the source to the audio output
}

var audioCtx = new (window.AudioContext || window.webkitAudioContext)();

var count=0;
function onWsMessage( msg ){ 
   if (count ==0){
      startTime = audioCtx.currentTime;
  
    //var source = audioCtx.createBufferSource(); 
    
    //var buffer = new Uint8Array(msg.data);
    //var buffer=base64ToBuffer(msg.data)


    var data=new Int16Array(msg.data)
    var frameCount = audioCtx.sampleRate
    var myArrayBuffer = audioCtx.createBuffer(1, msg.data.byteLength,  frameCount);

    //context.decodeAudioData(
    //var audioCtx = new AudioContext();
    var source = audioCtx.createBufferSource();
    source.connect(audioCtx.destination);
    var channels = 1;
    
   
    var c=0
    var n=0.5
    for (var channel = 0; channel < channels; channel++) {
     // This gives us the actual ArrayBuffer that contains the data
     var nowBuffering = myArrayBuffer.getChannelData(channel);
     for (var i = 0; i < msg.data.byteLength; i++) {
       // Math.random() is in [0; 1.0]
       // audio needs to be in [-1.0; 1.0]
       //65535
       if (isNaN(data[i*n] / 0xFFFF)){
        nowBuffering[c++] = 0
       }
       else{
        nowBuffering[c++] =  data[i*n] / 0xFFFF;
       }
        
    
     }
     console.log("eee",msg.data.byteLength/n,c,nowBuffering)
    }

    //------------------------------
    // Fill the buffer with white noise;
    // just random values between -1.0 and 1.0
    /*
    for (var channel = 0; channel < buffer.numberOfChannels; channel++) {
      // This gives us the actual ArrayBuffer that contains the data
      var nowBuffering = myArrayBuffer.getChannelData(channel);
      console.log("aaaaaa",nowBuffering)
      for (var i = 0; i < buffer.length; i++) {
        // Math.random() is in [0; 1.0]
        // audio needs to be in [-1.0; 1.0]
        console.log("ppp")
        nowBuffering[c++] =buffer[i];
      }
    }
    */
    //-----------------------------------
    
    
    if (source){

      source.start(0)
      source.buffer=myArrayBuffer
      
   
      }
    

    //source.buffer=myArrayBuffer
      //source.buffer=buffer
    }
  
    
    
    //var source = audioCtx.createMediaStreamSource(stream);
    //audio.setMediaStreamSource( stream );
    ///console.log(buffer)
    //console.log(toUTF8Array(msg.data.slice(2,-1)))

      
 

}
wsh.binaryType = 'arraybuffer';
wsh.onmessage = onWsMessage;


var ap = new OpusEncoderProcessor( wsh );
var mh = new MediaHandler( ap );

function sendSettings()
{
    if( document.getElementById( "encode" ).checked )
	encode = 1;
    else
	encode = 0;

    var rate = String( mh.context.sampleRate / ap.downSample );
    var opusRate = String( ap.opusRate );
    var opusFrameDur = String( ap.opusFrameDur )

    var msg = "m:" + JSON.stringify({"action":"init",
                      "data":[ parseInt(rate), parseInt(encode), parseInt(opusRate), parseInt(opusFrameDur) ],
                      "headers":{}});
    wsh.send( msg );
}

function startRecord()
{
    document.getElementById( "record").innerHTML = "Stop";
    document.getElementById( "encode" ).disabled = true;
    mh.context.resume(); // needs an await?
    sendSettings();
    isRecording = true;
    console.log( 'started recording' );
    // Get an AudioBufferSourceNode.
    // This is the AudioNode to use when we want to play an AudioBuffer
    //source = audioCtx.createBufferSource();

    /*
    var myArrayBuffer = audioCtx.createBuffer(2, audioCtx.sampleRate * 3, audioCtx.sampleRate);
    // Fill the buffer with white noise;
    // just random values between -1.0 and 1.0
    for (var channel = 0; channel < myArrayBuffer.numberOfChannels; channel++) {
      // This gives us the actual ArrayBuffer that contains the data
      var nowBuffering = myArrayBuffer.getChannelData(channel);
      for (var i = 0; i < myArrayBuffer.length; i++) {
        // Math.random() is in [0; 1.0]
        // audio needs to be in [-1.0; 1.0]
        nowBuffering[i] = Math.random() * 2 - 1;
      }
    }

    // set the buffer in the AudioBufferSourceNode
    source.buffer = myArrayBuffer;
    */
    // connect the AudioBufferSourceNode to the
    // destination so we can hear the sound
    
    // start the source playing
   
}

function stopRecord()
{
    isRecording  = false;
    document.getElementById( "record").innerHTML = "Record";
    document.getElementById( "encode" ).disabled = false;
    console.log( 'ended recording' );    
}

function toggleRecord()
{
    if( isRecording )
	stopRecord();
    else
	startRecord();
}

['ALCError', 
'ALC_ALL_ATTRIBUTES', 
'ALC_ATTRIBUTES_SIZE', 
'ALC_CAPTURE_DEFAULT_DEVICE_SPECIFIER', 
'ALC_CAPTURE_DEVICE_SPECIFIER', 
'ALC_CAPTURE_SAMPLES', 
'ALC_DEFAULT_DEVICE_SPECIFIER', 
'ALC_DEVICE_SPECIFIER', 
'ALC_EXTENSIONS', 
'ALC_FALSE', 
'ALC_FREQUENCY', 
'ALC_INVALID_CONTEXT', 
'ALC_INVALID_DEVICE', 
'ALC_INVALID_ENUM', 
'ALC_INVALID_VALUE', 
'ALC_MAJOR_VERSION', 
'ALC_MINOR_VERSION', 
'ALC_MONO_SOURCES', 
'ALC_NO_ERROR', 
'ALC_OUT_OF_MEMORY', 
'ALC_REFRESH', 
'ALC_STEREO_SOURCES', 
'ALC_SYNC', 
'ALC_TRUE', 
'ALError', 
'AL_BITS', 
'AL_BUFFER', 
'AL_BUFFERS_PROCESSED', 
'AL_BUFFERS_QUEUED', 
'AL_BYTE_OFFSET', 
'AL_CHANNELS', 
'AL_CONE_INNER_ANGLE', 
'AL_CONE_OUTER_ANGLE', 
'AL_CONE_OUTER_GAIN', 
'AL_DIRECTION', 
'AL_DISTANCE_MODEL', 
'AL_DOPPLER_FACTOR', 
'AL_DOPPLER_VELOCITY', 
'AL_EXPONENT_DISTANCE', 
'AL_EXPONENT_DISTANCE_CLAMPED', 
'AL_EXTENSIONS', 
'AL_FALSE', 
'AL_FORMAT_MONO16', 
'AL_FORMAT_MONO8', 
'AL_FORMAT_STEREO16', 
'AL_FORMAT_STEREO8', 
'AL_FREQUENCY', 
'AL_GAIN', 
'AL_INITIAL', 
'AL_INVALID_ENUM', 
'AL_INVALID_NAME', 
'AL_INVALID_OPERATION', 
'AL_INVALID_VALUE', 
'AL_INVERSE_DISTANCE', 
'AL_INVERSE_DISTANCE_CLAMPED', 
'AL_LINEAR_DISTANCE', 
'AL_LINEAR_DISTANCE_CLAMPED', 
'AL_LOOPING', 
'AL_MAX_DISTANCE', 
'AL_MAX_GAIN', 
'AL_MIN_GAIN', 
'AL_NONE', 
'AL_NO_ERROR', 
'AL_ORIENTATION', 
'AL_OUT_OF_MEMORY', 
'AL_PAUSED', 
'AL_PENDING', 
'AL_PITCH', 
'AL_PLAYING', 
'AL_POSITION', 
'AL_PROCESSED', 
'AL_REFERENCE_DISTANCE', 
'AL_RENDERER', 
'AL_ROLLOFF_FACTOR', 
'AL_SAMPLE_OFFSET', 
'AL_SEC_OFFSET', 
'AL_SIZE', 
'AL_SOURCE_RELATIVE', 
'AL_SOURCE_STATE', 
'AL_SOURCE_TYPE', 
'AL_SPEED_OF_SOUND', 
'AL_STATIC', 
'AL_STOPPED', 
'AL_STREAMING', 
'AL_TRUE', 
'AL_UNDETERMINED', 
'AL_UNUSED', 
'AL_VELOCITY', 
'AL_VENDOR', 
'AL_VERSION', 
'ALboolean', 
'ALbyte', 
'ALchar', 
'ALdouble', 
'ALenum', 
'ALfloat', 
'ALint', 
'ALshort', 
'ALsizei', 
'ALubyte', 
'ALuint', 
'ALushort', 
'Buffer', 
'Listener', 
'MAX_FLOAT', 
'OAL_DONT_AUTO_INIT', 
'OAL_STREAM_BUFFER_COUNT', 
'OalError', 
'OalWarning', 
'PYOGG_AVAIL', 
'Source', 
'SourceStream', 
'StreamBuffer', 
'WAVE_AVAIL', 
'WAVE_STREAM_BUFFER_SIZE', 
'WaveFile', 
'WaveFileStream', 
'__builtins__', 
'__cached__', 
'__doc__', 
'__file__', 
'__loader__', 
'__name__', 
'__package__', 
'__path__', 
'__spec__', 
'_alError', 
'_buffers', 
'_channels_to_al', 
'_check', 
'_err', 
'_format_enum', 
'_listener', 
'_no_pyogg_error', 
'_oalcontext', 
'_oaldevice', 
'_sources', 
'_to_c_int', 
'_to_c_uint', 
'_to_int', 
'_to_val', 
'al', 
'alBuffer3f', 
'alBuffer3i', 
'alBufferData', 
'alBufferf', 
'alBufferfv', 
'alBufferi', 
'alBufferiv', 
'alDeleteBuffers', 
'alDeleteSources', 
'alDisable', 
'alDistanceModel', 
'alDopplerFactor', 
'alDopplerVelocity', 
'alEnable', 
'alGenBuffers', 
'alGenSources', 
'alGetBoolean', 
'alGetBooleanv', 
'alGetBuffer3f', 
'alGetBuffer3i', 
'alGetBufferf', 
'alGetBufferfv', 
'alGetBufferi', 
'alGetBufferiv', 
'alGetDouble', 
'alGetDoublev', 
'alGetEnumValue', 
'alGetError', 
'alGetFloat', 
'alGetFloatv', 
'alGetInteger', 
'alGetIntegerv', 
'alGetListener3f', 
'alGetListener3i', 
'alGetListenerf', 
'alGetListenerfv', 
'alGetListeneri', 
'alGetListeneriv', 
'alGetProcAddress', 
'alGetSource3f', 
'alGetSource3i', 
'alGetSourcef', 
'alGetSourcefv', 
'alGetSourcei', 
'alGetSourceiv', 
'alGetString', 
'alIsBuffer', 
'alIsEnabled', 
'alIsExtensionPresent', 
'alIsSource', 
'alListener3f', 
'alListener3i', 
'alListenerf', 
'alListenerfv', 
'alListeneri', 
'alListeneriv', 
'alSource3f', 
'alSource3i', 
'alSourcePause', 
'alSourcePausev', 
'alSourcePlay', 
'alSourcePlayv', 
'alSourceQueueBuffers', 
'alSourceRewind', 
'alSourceRewindv', 
'alSourceStop', 
'alSourceStopv', 
'alSourceUnqueueBuffers', 
'alSourcef', 
'alSourcefv', 
'alSourcei', 
'alSourceiv', 
'alSpeedOfSound', 
'al_check_error', 
'al_enums', 
'al_lib', 
'alc', 
'alcCaptureCloseDevice', 
'alcCaptureOpenDevice', 
'alcCaptureSamples', 
'alcCaptureStart', 
'alcCaptureStop', 
'alcCloseDevice', 
'alcCreateContext', 
'alcDestroyContext', 
'alcGetContextsDevice', 
'alcGetCurrentContext', 
'alcGetEnumValue', 
'alcGetError', 
'alcGetIntegerv', 
'alcGetProcAddress', 
'alcGetString', 
'alcIsExtensionPresent', 
'alcMakeContextCurrent', 
'alcOpenDevice', 
'alcProcessContext', 
'alcSuspendContext', 
'alc_check_error', 
'alc_enums', 
'ctypes', 
'k', 
'lib', 
'library_loader', 
'local_items', 
'long', 
'oalGetALCEnum', 
'oalGetALEnum', 
'oalGetContext', 
'oalGetDevice', 
'oalGetEnum', 
'oalGetInit', 
'oalGetListener', 
'oalInit', 
'oalOpen', 
'oalQuit', 
'oalSetAutoInit', 
'oalSetStreamBufferCount', 
'oalStream', 
'os', 
'sys', 
'tuple_add3', 
'v', 
'warnings', 
'wave', 
'waveSetStreamBufferSize']